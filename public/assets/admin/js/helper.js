function submitData(formId, targetURL, fieldId) {
    $(".error_message").html("");
    $("#success-box").hide();
    $("#error-box").hide();
    var form = $("#" + formId);
    var data = form.serialize();
    $.ajax({
        type: "POST",
        url: targetURL,
        data: data,
        form: form,
        dataType: "json",
    })
        .done(function (response) {
            $("#" + fieldId).html(response.data);
            $(".loader").addClass("is-active");
            $("#success-box").show();
            setTimeout(function () {
                $("#success-box").hide();
                $(".loader").removeClass("is-active");
            }, 1000);
        })
        .fail(function (jqXHR, textStatus, error) {
            if (fieldId != "couponDetails") {
                $("#error-box").show();
                setTimeout(function () {
                    $("#error-box").hide();
                    $(".loader").removeClass("is-active");
                }, 3000);
                if (jqXHR.status === 422) {
                    //parseErrors(jqXHR);
                } else {
                }
            }
        });
    return false;
}

function getErrors(jqXhr) {
    if (jqXhr.status === 422) {
        console.log(jqXhr.responseJSON["errors"]);
        var errors = jqXhr.responseJSON["errors"];
        errorsHtml = '<div class="alert alert-danger"><ul>';
        $.each(errors, function (key, value) {
            errorsHtml += "<li>" + value[0] + "</li>"; //showing only the first error.
        });
        errorsHtml += "</ul></div>";
        $("#form-errors").html(errorsHtml);
        $("#form-errors").show();
    }
    setTimeout(function () {
        $("#form-errors").hide();
    }, 5000);
}

function deleteRecord(id, formId, targetURL) {
    Swal.fire({
        title: "Delete!",
        text: "Are you sure you want to delete this record?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, do it!",
    }).then((result) => {
        if (result.isConfirmed) {
            var form = $("#" + formId + "_" + id);
            var data = form.serialize();
            var redirectTo = targetURL.substr(0, targetURL.lastIndexOf("/"));
            $.ajax({
                type: "POST",
                url: targetURL,
                data: data,
                form: form,
                dataType: "json",
            })
                .done(function (data) { })
                .fail(function (jqXHR, textStatus, error) {
                    if (jqXHR.status !== 200) {
                        Swal.fire({
                            icon: "error",
                            title: "Oops...",
                            text: jqXHR.responseJSON.message,
                        });
                    } else {
                        Swal.fire({
                            title: "Deleted!",
                            text: "Record has been deleted successfully.",
                            icon: "success",
                        }).then((result) => {
                            if (result.isConfirmed) {
                                location.href = redirectTo;
                            }
                        });
                    }
                });
            return false;
        }
    });
}

function changeStatus(id, formId, targetURL) {
    Swal.fire({
        title: "Update!",
        text: "Are you sure you want to update status of this record?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, do it!",
    }).then((result) => {
        if (result.isConfirmed) {
            var form = $("#" + formId + "_" + id);
            var data = form.serialize();
            var redirectTo = targetURL.substr(
                0,
                targetURL.lastIndexOf("/", targetURL.lastIndexOf("/") - 1)
            );
            $.ajax({
                type: "POST",
                url: targetURL,
                data: data,
                form: form,
                dataType: "json",
            })
                .done(function (data) { })
                .fail(function (jqXHR, textStatus, error) {
                    if (jqXHR.status !== 200) {
                        Swal.fire({
                            icon: "error",
                            title: "Oops...",
                            text: jqXHR.responseJSON.message,
                        });
                    } else {
                        Swal.fire({
                            title: "Updated!",
                            text: "Record has been updated successfully.",
                            icon: "success",
                        }).then((result) => {
                            if (result.isConfirmed) {
                                location.href = redirectTo;
                            }
                        });
                    }
                });
            return false;
        }
    });
}
