@if (Session::has('message'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <h5><i class="icon fas fa-check"></i> Success!</h5>
        {{ Session::get('message') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
