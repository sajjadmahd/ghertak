@extends('layouts.admin.app')
@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Manage Variant Options</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('variantoptions.index') }}">Variant Options</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add Variant Option</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Add Variant Option</h2>

            </div>
            <div class="card-body">
                <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
                    @include('layouts.partials.admin.errors')
                    <form name="add_form" id="add_form" method="post" action="{{ route('variantoptions.store') }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-12 p-2">
                                <label for="variant_id">Variant <span class="text-danger">*</span></label>
                                <select name="variant_id" class="form-control">
                                    <option value="">Select</option>
                                    @foreach ($variants as $item)
                                        <option value="{{ $item->id }}"
                                            {{ $item->id == old('variant_id') ? ' selected' : '' }}>
                                            {{ $item->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-12 p-2">
                                <label for="title">Name <span class="text-danger">*</span></label>
                                <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                            </div>
                            <div class="col-md-12 p-2">
                                <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-footer"></div>
        </div>
    </section>

@endsection

@section('jsfooter')
@endsection
