@extends('layouts.admin.app')
@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Manage CMS Pages</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('cmspages.index') }}">CMS Pages</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add CMS Page</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Add CMS Page</h2>

            </div>
            <div class="card-body">
                <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
                    @include('layouts.partials.admin.errors')
                    <form name="add_form" id="add_form" method="post" action="{{ route('cmspages.store') }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-12 p-2">
                                <label for="parent_id">Parent Page <span class="text-danger">*</span></label>
                                <select name="parent_id" class="form-control">
                                    <option value="0">Root</option>
                                    @foreach ($parentPages as $item)
                                        <option value="{{ $item->id }}"
                                            {{ $item->id == old('parent_id') ? ' selected' : '' }}>
                                            {{ $item->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-12 p-2">
                                <label for="name">Name <span class="text-danger">*</span></label>
                                <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                            </div>

                            <div class="col-md-12 p-2">
                                <label for="short_description">Short Description <span
                                        class="text-danger">*</span></label>
                                <textarea name="short_description" id="short_description" class="form-control" cols="30"
                                    rows="10">{{ old('short_description') }}</textarea>
                            </div>
                            <div class="col-md-12 p-2">
                                <label for="long_description">Long Description <span class="text-danger">*</span></label>
                                <textarea name="long_description" id="long_description" class="form-control" cols="30"
                                    rows="10">{{ old('long_description') }}</textarea>
                            </div>
                            <div class="col-md-12 p-2">
                                <label for="page_meta_title">Meta Title <span class="text-danger">*</span></label>
                                <input type="text" name="page_meta_title" class="form-control"
                                    value="{{ old('page_meta_title') }}">
                            </div>
                            <div class="col-md-12 p-2">
                                <label for="page_meta_description">Meta Description <span
                                        class="text-danger">*</span></label>
                                <input type="text" name="page_meta_description" class="form-control"
                                    value="{{ old('page_meta_description') }}">
                            </div>
                            <div class="col-md-12 p-2">
                                <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-footer"></div>
        </div>
    </section>

@endsection

@section('jsfooter')
@endsection
