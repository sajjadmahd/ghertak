@extends('layouts.admin.app')
@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Manage Variants</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('variants.index') }}">Variants</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Variant</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>


    <section class="content">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Edit Variant</h2>

            </div>
            <div class="card-body">
                <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
                    @include('layouts.partials.admin.errors')
                    <form name="edit_form" id="edit_form" method="post"
                        action="{{ route('variants.update', $variant->id) }}">
                        @method('PUT')
                        @csrf
                        <div class="row">
                            <div class="col-md-12 p-2">
                                <label for="name">Name <span class="text-danger">*</span></label>
                                <input type="text" name="name" class="form-control"
                                    value="{{ old('name', $variant->name) }}">
                            </div>
                            <div class="col-md-12 p-2">
                                <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-footer"></div>
        </div>
    </section>


@endsection

@section('jsfooter')
@endsection
