@extends('layouts.admin.app')
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Manage Banners</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Banners</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="card">
            <div class="card-header">
                <div class="d-flex justify-content-between">
                    <h2 class="card-title">Banners</h2>
                    <a class="btn btn-outline-primary btn-sm" href="{{ route('banners.create') }}">
                        <i class="fa fa-plus"></i> Add Banner
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
                    @include('layouts.partials.admin.message')
                    @if (sizeof($data) > 0)
                        @include('layouts.partials.admin.paging')
                        <table class="table table-hover mt-3">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Link</th>
                                    <th style="width: 20%;">Updated On</th>
                                    <th style="width: 22%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $row)
                                    <tr>
                                        <td>
                                            <a href="#" data-toggle="tooltip" data-placement="right" data-html="true"
                                                title='<img style="max-width: 186px;" src="{{ asset($row->image_path) }}">'>
                                                <img class="img-thumbnail" style="width: 64px;"
                                                    src="{{ asset($row->image_path) }}" alt="">
                                            </a>
                                        </td>
                                        <td>{{ $row->image_link ?: 'NA' }}</td>
                                        <td>{{ Carbon\Carbon::parse($row->updated_at)->format('F j, Y, g:i a') }}</td>
                                        <td>
                                            <div class="d-flex">
                                                <div class="mr-2">
                                                    <form id="banner_status_{{ $row->id }}" action="" method="post">
                                                        @csrf
                                                        @method('PUT')
                                                        <input type="hidden" name="is_active"
                                                            value="{{ $row->is_active ? 'false' : 'true' }}" />
                                                        <button
                                                            class="btn btn-{{ $row->is_active ? 'success' : 'warning' }} btn-sm"
                                                            type="button"
                                                            onclick="javascript: changeStatus({{ $row->id }}, 'banner_status', '{{ route('banners.isActive', $row->id) }}');return false;">
                                                            {{ $row->is_active ? 'Enabled' : 'Disabled' }}
                                                        </button>
                                                    </form>
                                                </div>
                                                <div class="mr-2">
                                                    <a href="{{ route('banners.edit', $row->id) }}"
                                                        class="btn btn-primary btn-sm"><i class="fa fa-edit">
                                                            Edit</i></a>
                                                </div>
                                                <div class="p-0">
                                                    <form id="banner_delete_{{ $row->id }}" action="" method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="btn btn-danger btn-sm" type="button"
                                                            onclick="javascript: deleteRecord({{ $row->id }}, 'banner_delete', '{{ route('banners.destroy', $row->id) }}');return false;"><i
                                                                class="fa fa-trash"> Delete</i></button>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @include('layouts.partials.admin.paging')
                    @else
                        @include('layouts.partials.admin.noinfo')
                    @endif
                </div>
            </div>
            <div class="card-footer"></div>
        </div>
    </section>

@endsection
@section('jsfooter')

@endsection
