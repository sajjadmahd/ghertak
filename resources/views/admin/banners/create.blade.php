@extends('layouts.admin.app')
@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Manage Banners</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('banners.index') }}">Banners</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add Banner</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Add Banner</h2>

            </div>
            <div class="card-body">
                <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
                    @include('layouts.partials.admin.errors')
                    <form name="add_form" id="add_form" method="post" action="{{ route('banners.store') }}"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-12 p-2">
                                <label for="image_path">Image <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="image_path" id="exampleInputFile"
                                            accept=".jpeg,.png,.jpg,.gif,.svg">
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 p-2">
                                <label for="image_link">Link </label>
                                <input type="text" name="image_link" class="form-control"
                                    value="{{ old('image_link') }}">
                            </div>
                            <div class="col-md-4 p-2">
                                <label for="link_target">Target </label>
                                <select name="link_target" class="form-control">
                                    <option value="_self" {{ old('link_target') == '_self' ? ' selected' : '' }}>
                                        Internal
                                    </option>
                                    <option value="_blank" {{ old('link_target') == '_blank' ? ' selected' : '' }}>
                                        External
                                    </option>
                                </select>
                            </div>
                            <div class="col-md-12 p-2">
                                <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-footer"></div>
        </div>
    </section>

@endsection

@section('jsfooter')
@endsection
