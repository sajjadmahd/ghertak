@extends('layouts.admin.app')
@section('css-section')
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Manage Simple Products</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('products.index') }}">Products</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add Simple Product</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Add Simple Product</h2>
            </div>
            <div class="card-body">
                <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
                    @include('layouts.partials.admin.errors')
                    <form name="add_form" id="add_form" method="post" action="{{ route('simple-product.store') }}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="product_type" id="product_type" value="simple">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 p-2">
                                        <label for="name">Name <span class="text-danger">*</span></label>
                                        <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="sku">SKU <span class="text-danger">*</span></label>
                                        <input type="text" name="sku" class="form-control" value="{{ old('sku') }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="price">Price <span class="text-danger">*</span></label>
                                        <input type="text" name="price" class="form-control"
                                            value="{{ old('price') }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="available_stock">Available Stock <span
                                                class="text-danger">*</span></label>
                                        <input type="text" name="available_stock" class="form-control"
                                            value="{{ old('available_stock') }}">
                                    </div>
                                    <div class="col-md-12 p-2">
                                        <label for="details">Details <span class="text-danger">*</span></label>
                                        <textarea name="details" id="details" cols="30"
                                            rows="10">{{ old('details') }}</textarea>
                                    </div>
                                    <div class="col-md-12 p-2">
                                        <label for="preview_img">Preview Image <span class="text-danger">*</span></label>
                                        <input type="file" class="form-control" name="preview_img" id="preview_img" accept=".jpeg,.png,.jpg,.gif,.svg" >
                                    </div>
                                    <div class="col-md-12 p-2">
                                        <button type="submit" id="submit" name="submit"
                                            class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-header bg-primary">
                                                <h3 class="card-title">Categories</h3>
                                            </div>
                                            <div class="card-body">
                                                @foreach ($categories as $category)
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="checkbox"
                                                            id="categoryCheckbox{{ $category->id }}"
                                                            value="{{ $category->id }}" name="category_id[]">
                                                        <label class="form-check-label"
                                                            for="categoryCheckbox{{ $category->id }}">{{ $category->name }}</label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-header bg-primary">
                                                <h3 class="card-title">Brands</h3>
                                            </div>
                                            <div class="card-body">
                                                @foreach ($brands as $brand)
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="checkbox"
                                                            id="brandCheckbox{{ $brand->id }}"
                                                            value="{{ $brand->id }}" name="brand_id[]">
                                                        <label class="form-check-label"
                                                            for="brandCheckbox{{ $brand->id }}">{{ $brand->name }}</label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-footer"></div>
        </div>
    </section>

@endsection

@section('js-section')
    <script>
        $(function() {
            // Summernote
            $('#details').summernote()

            // CodeMirror
            CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
                mode: "htmlmixed",
                theme: "monokai"
            });
        })
    </script>
@endsection
