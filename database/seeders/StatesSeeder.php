<?php

namespace Database\Seeders;

use App\Models\State;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class StatesSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        State::truncate();

        $pakistan_states_list = array("Federal", "Punjab", "Sindh", "Khyber Pakhtunkhwa", "Balochistan", "Gilgit-Baltistan", "Azad Kashmir");

        foreach ($pakistan_states_list as $row) {
            State::insert(
                [
                    "country_id" => 164,
                    "name" => $row,
                    "slug" => Str::slug($row),
                    "created_at" => Carbon::now(),
                    "updated_at" => Carbon::now(),
                ]
            );
        }
    }
}
