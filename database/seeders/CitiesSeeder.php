<?php

namespace Database\Seeders;

use App\Models\City;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CitiesSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        City::truncate();

        $pakistan_cities_list = array(
            0 =>
            array(
                'name' => 'Karachi',
                'state_id' => 3,
            ),
            1 =>
            array(
                'name' => 'Lahore',
                'state_id' => 2
            ),
            2 =>
            array(
                'name' => 'Faisalabad',
                'state_id' => 2
            ),
            3 =>
            array(
                'name' => 'Rawalpindi',
                'state_id' => 2
            ),
            4 =>
            array(
                'name' => 'Gujranwala',
                'state_id' => 2
            ),
            5 =>
            array(
                'name' => 'Peshawar',
                'state_id' => 4
            ),
            6 =>
            array(
                'name' => 'Multan',
                'state_id' => 2
            ),
            7 =>
            array(
                'name' => 'Saidu Sharif',
                'state_id' => 4
            ),
            8 =>
            array(
                'name' => 'Hyderabad City',
                'state_id' => 3,
            ),
            9 =>
            array(
                'name' => 'Islamabad',
                'state_id' => 1
            ),
            10 =>
            array(
                'name' => 'Quetta',
                'state_id' => 5
            ),
            11 =>
            array(
                'name' => 'Bahawalpur',
                'state_id' => 2
            ),
            12 =>
            array(
                'name' => 'Sargodha',
                'state_id' => 2
            ),
            13 =>
            array(
                'name' => 'Sialkot City',
                'state_id' => 2
            ),
            14 =>
            array(
                'name' => 'Sukkur',
                'state_id' => 3,
            ),
            15 =>
            array(
                'name' => 'Larkana',
                'state_id' => 3,
            ),
            16 =>
            array(
                'name' => 'Chiniot',
                'state_id' => 2
            ),
            17 =>
            array(
                'name' => 'Shekhupura',
                'state_id' => 2
            ),
            18 =>
            array(
                'name' => 'Jhang City',
                'state_id' => 2
            ),
            19 =>
            array(
                'name' => 'Dera Ghazi Khan',
                'state_id' => 2
            ),
            20 =>
            array(
                'name' => 'Gujrat',
                'state_id' => 2
            ),
            21 =>
            array(
                'name' => 'Rahimyar Khan',
                'state_id' => 2
            ),
            22 =>
            array(
                'name' => 'Kasur',
                'state_id' => 2
            ),
            23 =>
            array(
                'name' => 'Mardan',
                'state_id' => 4
            ),
            24 =>
            array(
                'name' => 'Mingaora',
                'state_id' => 4
            ),
            25 =>
            array(
                'name' => 'Nawabshah',
                'state_id' => 3,
            ),
            26 =>
            array(
                'name' => 'Sahiwal',
                'state_id' => 2
            ),
            27 =>
            array(
                'name' => 'Mirpur Khas',
                'state_id' => 3,
            ),
            28 =>
            array(
                'name' => 'Okara',
                'state_id' => 2
            ),
            29 =>
            array(
                'name' => 'Mandi Burewala',
                'state_id' => 2
            ),
            30 =>
            array(
                'name' => 'Jacobabad',
                'state_id' => 3,
            ),
            31 =>
            array(
                'name' => 'Saddiqabad',
                'state_id' => 2
            ),
            32 =>
            array(
                'name' => 'Kohat',
                'state_id' => 4
            ),
            33 =>
            array(
                'name' => 'Muridke',
                'state_id' => 2
            ),
            34 =>
            array(
                'name' => 'Muzaffargarh',
                'state_id' => 2
            ),
            35 =>
            array(
                'name' => 'Khanpur',
                'state_id' => 2
            ),
            36 =>
            array(
                'name' => 'Gojra',
                'state_id' => 2
            ),
            37 =>
            array(
                'name' => 'Mandi Bahauddin',
                'state_id' => 2
            ),
            38 =>
            array(
                'name' => 'Abbottabad',
                'state_id' => 4
            ),
            39 =>
            array(
                'name' => 'Turbat',
                'state_id' => 5
            ),
            40 =>
            array(
                'name' => 'Dadu',
                'state_id' => 3,
            ),
            41 =>
            array(
                'name' => 'Bahawalnagar',
                'state_id' => 2
            ),
            42 =>
            array(
                'name' => 'Khuzdar',
                'state_id' => 5
            ),
            43 =>
            array(
                'name' => 'Pakpattan',
                'state_id' => 2
            ),
            44 =>
            array(
                'name' => 'Tando Allahyar',
                'state_id' => 3,
            ),
            45 =>
            array(
                'name' => 'Ahmadpur East',
                'state_id' => 2
            ),
            46 =>
            array(
                'name' => 'Vihari',
                'state_id' => 2
            ),
            47 =>
            array(
                'name' => 'Jaranwala',
                'state_id' => 2
            ),
            48 =>
            array(
                'name' => 'New Mirpur',
                'state_id' => 7
            ),
            49 =>
            array(
                'name' => 'Kamalia',
                'state_id' => 2
            ),
            50 =>
            array(
                'name' => 'Kot Addu',
                'state_id' => 2
            ),
            51 =>
            array(
                'name' => 'Nowshera',
                'state_id' => 4
            ),
            52 =>
            array(
                'name' => 'Swabi',
                'state_id' => 4
            ),
            53 =>
            array(
                'name' => 'Khushab',
                'state_id' => 2
            ),
            54 =>
            array(
                'name' => 'Dera Ismail Khan',
                'state_id' => 4
            ),
            55 =>
            array(
                'name' => 'Chaman',
                'state_id' => 5
            ),
            56 =>
            array(
                'name' => 'Charsadda',
                'state_id' => 4
            ),
            57 =>
            array(
                'name' => 'Kandhkot',
                'state_id' => 3,
            ),
            58 =>
            array(
                'name' => 'Chishtian',
                'state_id' => 2
            ),
            59 =>
            array(
                'name' => 'Hasilpur',
                'state_id' => 2
            ),
            60 =>
            array(
                'name' => 'Attock Khurd',
                'state_id' => 2
            ),
            61 =>
            array(
                'name' => 'Muzaffarabad',
                'state_id' => 7
            ),
            62 =>
            array(
                'name' => 'Mianwali',
                'state_id' => 2
            ),
            63 =>
            array(
                'name' => 'Jalalpur Jattan',
                'state_id' => 2
            ),
            64 =>
            array(
                'name' => 'Bhakkar',
                'state_id' => 2
            ),
            65 =>
            array(
                'name' => 'Zhob',
                'state_id' => 5
            ),
            66 =>
            array(
                'name' => 'Dipalpur',
                'state_id' => 2
            ),
            67 =>
            array(
                'name' => 'Kharian',
                'state_id' => 2
            ),
            68 =>
            array(
                'name' => 'Mian Channun',
                'state_id' => 2
            ),
            69 =>
            array(
                'name' => 'Bhalwal',
                'state_id' => 2
            ),
            70 =>
            array(
                'name' => 'Jamshoro',
                'state_id' => 3,
            ),
            71 =>
            array(
                'name' => 'Pattoki',
                'state_id' => 2
            ),
            72 =>
            array(
                'name' => 'Harunabad',
                'state_id' => 2
            ),
            73 =>
            array(
                'name' => 'Kahror Pakka',
                'state_id' => 2
            ),
            74 =>
            array(
                'name' => 'Toba Tek Singh',
                'state_id' => 2
            ),
            75 =>
            array(
                'name' => 'Samundri',
                'state_id' => 2
            ),
            76 =>
            array(
                'name' => 'Shakargarh',
                'state_id' => 2
            ),
            77 =>
            array(
                'name' => 'Sambrial',
                'state_id' => 2
            ),
            78 =>
            array(
                'name' => 'Shujaabad',
                'state_id' => 2
            ),
            79 =>
            array(
                'name' => 'Hujra Shah Muqim',
                'state_id' => 2
            ),
            80 =>
            array(
                'name' => 'Kabirwala',
                'state_id' => 2
            ),
            81 =>
            array(
                'name' => 'Mansehra',
                'state_id' => 4
            ),
            82 =>
            array(
                'name' => 'Lala Musa',
                'state_id' => 2
            ),
            83 =>
            array(
                'name' => 'Chunian',
                'state_id' => 2
            ),
            84 =>
            array(
                'name' => 'Nankana Sahib',
                'state_id' => 2
            ),
            85 =>
            array(
                'name' => 'Bannu',
                'state_id' => 4
            ),
            86 =>
            array(
                'name' => 'Pasrur',
                'state_id' => 2
            ),
            87 =>
            array(
                'name' => 'Timargara',
                'state_id' => 4
            ),
            88 =>
            array(
                'name' => 'Parachinar',
                'state_id' => 4
            ),
            89 =>
            array(
                'name' => 'Chenab Nagar',
                'state_id' => 2
            ),
            90 =>
            array(
                'name' => 'Gwadar',
                'state_id' => 5
            ),
            91 =>
            array(
                'name' => 'Abdul Hakim',
                'state_id' => 2
            ),
            92 =>
            array(
                'name' => 'Hassan Abdal',
                'state_id' => 2
            ),
            93 =>
            array(
                'name' => 'Tank',
                'state_id' => 4
            ),
            94 =>
            array(
                'name' => 'Hangu',
                'state_id' => 4
            ),
            95 =>
            array(
                'name' => 'Risalpur Cantonment',
                'state_id' => 4
            ),
            96 =>
            array(
                'name' => 'Karak',
                'state_id' => 4
            ),
            97 =>
            array(
                'name' => 'Kundian',
                'state_id' => 2
            ),
            98 =>
            array(
                'name' => 'Umarkot',
                'state_id' => 3,
            ),
            99 =>
            array(
                'name' => 'Chitral',
                'state_id' => 4
            ),
            100 =>
            array(
                'name' => 'Dainyor',
                'state_id' => 6
            ),
            101 =>
            array(
                'name' => 'Kulachi',
                'state_id' => 4
            ),
            102 =>
            array(
                'name' => 'Kalat',
                'state_id' => 5
            ),
            103 =>
            array(
                'name' => 'Kotli',
                'state_id' => 7
            ),
            104 =>
            array(
                'name' => 'Gilgit',
                'state_id' => 6
            ),
            105 =>
            array(
                'name' => 'Narowal',
                'state_id' => 2
            ),
            106 =>
            array(
                'name' => 'Khairpur Mirs',
                'state_id' => 3,
            ),
            107 =>
            array(
                'name' => 'Khanewal',
                'state_id' => 2
            ),
            108 =>
            array(
                'name' => 'Jhelum',
                'state_id' => 2
            ),
            109 =>
            array(
                'name' => 'Haripur',
                'state_id' => 4
            ),
            110 =>
            array(
                'name' => 'Shikarpur',
                'state_id' => 3,
            ),
            111 =>
            array(
                'name' => 'Rawala Kot',
                'state_id' => 7
            ),
            112 =>
            array(
                'name' => 'Hafizabad',
                'state_id' => 2
            ),
            113 =>
            array(
                'name' => 'Lodhran',
                'state_id' => 2
            ),
            114 =>
            array(
                'name' => 'Malakand',
                'state_id' => 4
            ),
            115 =>
            array(
                'name' => 'Attock City',
                'state_id' => 2
            ),
            116 =>
            array(
                'name' => 'Batgram',
                'state_id' => 4
            ),
            117 =>
            array(
                'name' => 'Matiari',
                'state_id' => 3,
            ),
            118 =>
            array(
                'name' => 'Ghotki',
                'state_id' => 3,
            ),
            119 =>
            array(
                'name' => 'Naushahro Firoz',
                'state_id' => 3,
            ),
            120 =>
            array(
                'name' => 'Alpurai',
                'state_id' => 4
            ),
            121 =>
            array(
                'name' => 'Bagh',
                'state_id' => 7
            ),
            122 =>
            array(
                'name' => 'Daggar',
                'state_id' => 4
            ),
            123 =>
            array(
                'name' => 'Leiah',
                'state_id' => 2
            ),
            124 =>
            array(
                'name' => 'Tando Muhammad Khan',
                'state_id' => 3,
            ),
            125 =>
            array(
                'name' => 'Chakwal',
                'state_id' => 2
            ),
            126 =>
            array(
                'name' => 'Badin',
                'state_id' => 3,
            ),
            127 =>
            array(
                'name' => 'Lakki',
                'state_id' => 4
            ),
            128 =>
            array(
                'name' => 'Rajanpur',
                'state_id' => 2
            ),
            129 =>
            array(
                'name' => 'Dera Allahyar',
                'state_id' => 5
            ),
            130 =>
            array(
                'name' => 'Shahdad Kot',
                'state_id' => 3,
            ),
            131 =>
            array(
                'name' => 'Pishin',
                'state_id' => 5
            ),
            132 =>
            array(
                'name' => 'Sanghar',
                'state_id' => 3,
            ),
            133 =>
            array(
                'name' => 'Upper Dir',
                'state_id' => 4
            ),
            134 =>
            array(
                'name' => 'Thatta',
                'state_id' => 3,
            ),
            135 =>
            array(
                'name' => 'Dera Murad Jamali',
                'state_id' => 5
            ),
            136 =>
            array(
                'name' => 'Kohlu',
                'state_id' => 5
            ),
            137 =>
            array(
                'name' => 'Mastung',
                'state_id' => 5
            ),
            138 =>
            array(
                'name' => 'Dasu',
                'state_id' => 4
            ),
            139 =>
            array(
                'name' => 'Athmuqam',
                'state_id' => 7
            ),
            140 =>
            array(
                'name' => 'Loralai',
                'state_id' => 5
            ),
            141 =>
            array(
                'name' => 'Barkhan',
                'state_id' => 5
            ),
            142 =>
            array(
                'name' => 'Musa Khel Bazar',
                'state_id' => 5
            ),
            143 =>
            array(
                'name' => 'Ziarat',
                'state_id' => 5
            ),
            144 =>
            array(
                'name' => 'Gandava',
                'state_id' => 5
            ),
            145 =>
            array(
                'name' => 'Sibi',
                'state_id' => 5
            ),
            146 =>
            array(
                'name' => 'Dera Bugti',
                'state_id' => 5
            ),
            147 =>
            array(
                'name' => 'Eidgah',
                'state_id' => 6
            ),
            148 =>
            array(
                'name' => 'Uthal',
                'state_id' => 5
            ),
            150 =>
            array(
                'name' => 'Chilas',
                'state_id' => 6
            ),
            151 =>
            array(
                'name' => 'Panjgur',
                'state_id' => 5
            ),
            152 =>
            array(
                'name' => 'Gakuch',
                'state_id' => 6
            ),
            153 =>
            array(
                'name' => 'Qila Saifullah',
                'state_id' => 5
            ),
            154 =>
            array(
                'name' => 'Kharan',
                'state_id' => 5
            ),
            155 =>
            array(
                'name' => 'Aliabad',
                'state_id' => 6
            ),
            156 =>
            array(
                'name' => 'Awaran',
                'state_id' => 5
            ),
            157 =>
            array(
                'name' => 'Dalbandin',
                'state_id' => 5
            ),
        );

        foreach ($pakistan_cities_list as $row) {
            City::insert(
                [
                    "country_id" => 164,
                    "state_id" => $row["state_id"],
                    "name" => $row["name"],
                    "slug" => Str::slug($row["name"]),
                    "created_at" => Carbon::now(),
                    "updated_at" => Carbon::now(),
                ]
            );
        }
    }
}
