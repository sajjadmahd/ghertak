<?php

namespace Database\Seeders;

use App\Models\Store;
use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'email'    => 'sajjadmahd@gmail.com',
                'name'     => 'Super Admin',
                'password' => 'admin',
                'role'     => 'admin'
            ]
        ];

        $role = new Role;
        foreach ($users as $key => $user) {
            $newUser = User::updateOrCreate([
                'email' => $user['email']
            ], [
                'name'     => $user['name'],
                'password' => $user['password']
            ]);

            if ($newUser->id == 1) {
                $role = $role->updateOrCreate(['name' => 'admin']);
            } elseif ($newUser->id == 2) {
                $role = $role->updateOrCreate(['name' => 'seller']);
            } else {
                $role = $role->updateOrCreate(['name' => 'buyer']);
            }

            $permissions = Permission::pluck('id')->toArray();

            $role->syncPermissions($permissions);
            $newUser->assignRole([$role->id]);

            //Now we need to create a store agains this user
            $newStore = new Store();
            $newStore->user_id = $newUser->id;
            $newStore->unique_store_id = substr(md5(mt_rand()), 0, 5);
            $newStore->name = "Super Admin - Default";
            $newStore->slug = Str::slug("Super Admin - Default-") . $newStore->unique_store_id;
            $newStore->country_id = 0;
            $newStore->state_id = 0;
            $newStore->city_id = 0;
            $newStore->save();
        }
    }
}
