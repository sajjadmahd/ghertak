<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_images', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id')->unsigned();
            $table->bigInteger('product_variant_option_id')->unsigned();
            $table->string('small_img')->nullable();
            $table->string('medium_img')->nullable();
            $table->string('large_img')->nullable();
            $table->boolean('is_featured')->default(false);
            $table->timestamps();

            //foreign key constraint
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('product_variant_option_id')->references('id')->on('product_variant_options');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_images');
    }
}
