<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_stocks', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_combination_id')->unsigned();
            $table->integer('total_stock');
            $table->decimal('unit_price', 8, 2);
            $table->decimal('total_price', 12, 2);
            $table->timestamps();

            //foreign key constraint
            $table->foreign('product_combination_id')->references('id')->on('product_combinations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_stocks');
    }
}
