<?php

use App\Http\Controllers\Admin\BannerController;
use App\Http\Controllers\Admin\BrandController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CmsPageController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\SimpleProductController;
use App\Http\Controllers\Admin\VariantController;
use App\Http\Controllers\Admin\VariantOptionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin'], function () {
    Route::middleware(['auth'])->group(function () {
        Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard')->middleware('auth');
        Route::put('categories/{id}/is-active', [CategoryController::class, 'isActive'])->name('categories.isActive');
        Route::resource('categories', CategoryController::class);
        Route::put('brands/{id}/is-active', [BrandController::class, 'isActive'])->name('brands.isActive');
        Route::resource('brands', BrandController::class);
        Route::put('banners/{id}/is-active', [BannerController::class, 'isActive'])->name('banners.isActive');
        Route::resource('banners', BannerController::class);
        Route::resource('cmspages', CmsPageController::class);
        Route::put('cmspages/{id}/is-active', [CmsPageController::class, 'isActive'])->name('cmspages.isActive');
        Route::put('variants/{id}/is-active', [VariantController::class, 'isActive'])->name('variants.isActive');
        Route::resource('variants', VariantController::class);
        Route::put('variantoptions/{id}/is-active', [VariantOptionController::class, 'isActive'])->name('variantoptions.isActive');
        Route::resource('variantoptions', VariantOptionController::class);
        Route::get('products', [ProductController::class, 'index'])->name('products.index');
        Route::resource('simple-product', SimpleProductController::class);
    });
});
