<?php

namespace App\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['name', 'slug'];

    public function states()
    {
        return $this->hasMany(State::class);
    }
}
