<?php

namespace App\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class VariantOption extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['name', 'slug', 'variant_id'];

    protected static function boot()
    {
        parent::boot();

        static::created(function ($variantoption) {

            $variantoption->slug = $variantoption->createSlug($variantoption->name);

            $variantoption->save();
        });
    }

    public function variant()
    {
        return $this->belongsTo(Variant::class);
    }
}
