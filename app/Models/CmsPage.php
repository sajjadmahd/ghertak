<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class CmsPage extends BaseModel
{
    use SoftDeletes;

    protected static function boot()
    {
        parent::boot();

        static::created(function ($cmspage) {

            $cmspage->slug = $cmspage->createSlug($cmspage->name);

            $cmspage->save();
        });
    }

    public function parent()
    {
        return $this->belongsTo(CmsPage::class, 'parent_id');
    }
}
