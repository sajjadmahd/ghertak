<?php

namespace App\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Variant extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['name', 'slug'];

    protected static function boot()
    {
        parent::boot();

        static::created(function ($variant) {

            $variant->slug = $variant->createSlug($variant->name);

            $variant->save();
        });
    }

    public function options()
    {
        return $this->hasMany(VariantOption::class);
    }
}
