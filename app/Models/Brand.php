<?php

namespace App\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends BaseModel
{
    use SoftDeletes;

    protected static function boot()
    {
        parent::boot();

        static::created(function ($brand) {

            $brand->slug = $brand->createSlug($brand->name);

            $brand->save();
        });
    }
}
