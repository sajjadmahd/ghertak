<?php

namespace App\Repositories;

use App\Models\Brand;
use Illuminate\Support\Facades\DB;

class BrandRepository
{
    public function getAll($request)
    {
        $limit = env('DEFAULT_PAGING_LENGTH', 15);
        $brands = Brand::orderBy('updated_at', 'DESC')->paginate($limit)->setPath('');
        $brands->appends($request->all());

        return $brands;
    }

    public function getById($id)
    {
        return Brand::find($id);
    }

    public function create($request)
    {
        $brand = new Brand();
        $brand->name = $request->name;

        DB::transaction(function () use ($brand) {
            $brand->save();
        });

        return $brand;
    }

    public function update($id, $request)
    {
        $brand = $this->getById($id);
        $brand->name = $request->name;

        DB::transaction(function () use ($brand) {
            $brand->save();
        });

        return $brand;
    }

    public function delete($id)
    {
        $brand = $this->getById($id);

        DB::transaction(function () use ($brand) {
            $brand->delete();
        });

        return $brand;
    }

    public function isActive($id, $request)
    {
        $brand = $this->getById($id);
        $brand->is_active = filter_var($request->is_active, FILTER_VALIDATE_BOOLEAN);

        DB::transaction(function () use ($brand) {
            $brand->save();
        });

        return $brand;
    }
}
