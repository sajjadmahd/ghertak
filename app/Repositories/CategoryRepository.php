<?php

namespace App\Repositories;

use App\Models\Category;
use Illuminate\Support\Facades\DB;

class CategoryRepository
{
    public function getAll($request)
    {
        $limit = env('DEFAULT_PAGING_LENGTH', 15);
        $categories = Category::orderBy('updated_at', 'DESC')->paginate($limit)->setPath('');
        $categories->appends($request->all());

        return $categories;
    }

    public function getById($id)
    {
        return Category::find($id);
    }

    public function create($request)
    {
        $category = new Category();
        $category->parent_id = $request->parent_id;
        $category->name = $request->name;

        DB::transaction(function () use ($category) {
            $category->save();
        });

        return $category;
    }

    public function update($id, $request)
    {
        $category = $this->getById($id);
        $category->parent_id = $request->parent_id;
        $category->name = $request->name;

        DB::transaction(function () use ($category) {
            $category->save();
        });

        return $category;
    }

    public function delete($id)
    {
        $category = $this->getById($id);

        DB::transaction(function () use ($category) {
            $category->delete();
        });

        return $category;
    }

    public function isActive($id, $request)
    {
        $category = $this->getById($id);
        $category->is_active = filter_var($request->is_active, FILTER_VALIDATE_BOOLEAN);

        DB::transaction(function () use ($category) {
            $category->save();
        });

        return $category;
    }

    public function getAllParentCategories()
    {
        return Category::where('parent_id', 0)->get();
    }
}
