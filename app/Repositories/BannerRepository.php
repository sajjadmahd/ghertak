<?php

namespace App\Repositories;

use App\Models\Banner;
use Illuminate\Support\Facades\DB;

class BannerRepository
{
    public function getAll($request)
    {
        $limit = env('DEFAULT_PAGING_LENGTH', 15);
        $banners = Banner::orderBy('updated_at', 'DESC')->paginate($limit)->setPath('');
        $banners->appends($request->all());

        return $banners;
    }

    public function getById($id)
    {
        return Banner::find($id);
    }

    public function create($request)
    {
        $banner = new Banner();
        $banner->image_link = $request->image_link;
        $banner->link_target = $request->link_target;
        if ($request->hasFile('image_path')) {
            $banner->image_path = $this->uploadBanner($request);
        }

        DB::transaction(function () use ($banner) {
            $banner->save();
        });

        return $banner;
    }

    public function deleteBannerPhysically($image_path)
    {
        $pathToStore = 'assets/files/banners/';
        $destinationPath = public_path($pathToStore);
        if ($image_path != '' && file_exists($destinationPath . basename($image_path))) {
            unlink($destinationPath . basename($image_path));
        }
    }

    public function uploadBanner($request)
    {
        $imageFile = $request->file('image_path');
        $pathToStore = 'assets/files/banners/';
        $destinationPath = public_path($pathToStore);
        $originalFile = $imageFile->getClientOriginalName();
        $filename = strtotime(date('Y-m-d H:i:s')) . $originalFile;
        $imageFile->move($destinationPath, $filename);
        $image_path = $pathToStore . $filename;
        return $image_path;
    }

    public function update($id, $request)
    {
        $banner = $this->getById($id);

        if ($request->hasFile('image_path')) {
            $this->deleteBannerPhysically($banner->image_path);
            $banner->image_path = $this->uploadBanner($request);
        }

        $banner->image_link = $request->image_link;
        $banner->link_target = $request->link_target;

        DB::transaction(function () use ($banner) {
            $banner->save();
        });

        return $banner;
    }

    public function delete($id)
    {
        $banner = $this->getById($id);

        DB::transaction(function () use ($banner) {
            $this->deleteBannerPhysically($banner->image_path);
            $banner->delete();
        });

        return $banner;
    }

    public function isActive($id, $request)
    {
        $brand = $this->getById($id);
        $brand->is_active = filter_var($request->is_active, FILTER_VALIDATE_BOOLEAN);

        DB::transaction(function () use ($brand) {
            $brand->save();
        });

        return $brand;
    }
}
