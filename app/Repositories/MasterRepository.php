<?php

namespace App\Repositories;

use App\Models\Brand;
use App\Models\Category;

class MasterRepository
{
    public function getAllCategories()
    {
        return Category::where('is_active', 1)->get();
    }

    public function getAllBrands()
    {
        return Brand::where('is_active', 1)->get();
    }
}
