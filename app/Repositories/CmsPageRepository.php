<?php

namespace App\Repositories;

use App\Models\CmsPage;
use Illuminate\Support\Facades\DB;

class CmsPageRepository
{
    public function getAll($request)
    {
        $limit = env('DEFAULT_PAGING_LENGTH', 15);
        $cmsPages = CmsPage::orderBy('updated_at', 'DESC')->paginate($limit)->setPath('');
        $cmsPages->appends($request->all());

        return $cmsPages;
    }

    public function getById($id)
    {
        return CmsPage::find($id);
    }

    public function create($request)
    {
        $cmsPage = new CmsPage();
        $cmsPage->parent_id = $request->parent_id;
        $cmsPage->name = $request->name;
        $cmsPage->short_description = $request->short_description;
        $cmsPage->long_description = $request->long_description;
        $cmsPage->page_meta_title = $request->page_meta_title;
        $cmsPage->page_meta_description = $request->page_meta_description;

        DB::transaction(function () use ($cmsPage) {
            $cmsPage->save();
        });

        return $cmsPage;
    }

    public function update($id, $request)
    {
        $cmsPage = $this->getById($id);
        $cmsPage->parent_id = $request->parent_id;
        $cmsPage->name = $request->name;
        $cmsPage->short_description = $request->short_description;
        $cmsPage->long_description = $request->long_description;
        $cmsPage->page_meta_title = $request->page_meta_title;
        $cmsPage->page_meta_description = $request->page_meta_description;

        DB::transaction(function () use ($cmsPage) {
            $cmsPage->save();
        });


        return $cmsPage;
    }

    public function delete($id)
    {
        $cmsPage = $this->getById($id);

        DB::transaction(function () use ($cmsPage) {
            $cmsPage->delete();
        });

        return $cmsPage;
    }

    public function isActive($id, $request)
    {
        $cmsPage = $this->getById($id);
        $cmsPage->is_active = filter_var($request->is_active, FILTER_VALIDATE_BOOLEAN);

        DB::transaction(function () use ($cmsPage) {
            $cmsPage->save();
        });

        return $cmsPage;
    }

    public function getAllParentPages()
    {
        return CmsPage::where('parent_id', 0)->get();
    }
}
