<?php

namespace App\Repositories;

use App\Models\VariantOption;
use Illuminate\Support\Facades\DB;

class VariantOptionRepository
{
    public function getAll($request)
    {
        $limit = env('DEFAULT_PAGING_LENGTH', 15);
        $variantOptions = VariantOption::orderBy('updated_at', 'DESC')->paginate($limit)->setPath('');
        $variantOptions->appends($request->all());

        return $variantOptions;
    }

    public function getById($id)
    {
        return VariantOption::find($id);
    }

    public function create($request)
    {
        $variantOption = new VariantOption();
        $variantOption->variant_id = $request->variant_id;
        $variantOption->name = $request->name;

        DB::transaction(function () use ($variantOption) {
            $variantOption->save();
        });

        return $variantOption;
    }

    public function update($id, $request)
    {
        $variantOption = $this->getById($id);
        $variantOption->variant_id = $request->variant_id;
        $variantOption->name = $request->name;

        DB::transaction(function () use ($variantOption) {
            $variantOption->save();
        });

        return $variantOption;
    }

    public function delete($id)
    {
        $variantOption = $this->getById($id);

        DB::transaction(function () use ($variantOption) {
            $variantOption->delete();
        });

        return $variantOption;
    }

    public function isActive($id, $request)
    {
        $variantOption = $this->getById($id);
        $variantOption->is_active = filter_var($request->is_active, FILTER_VALIDATE_BOOLEAN);

        DB::transaction(function () use ($variantOption) {
            $variantOption->save();
        });

        return $variantOption;
    }
}
