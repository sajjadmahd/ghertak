<?php

namespace App\Repositories;

use App\Models\Variant;
use Illuminate\Support\Facades\DB;

class VariantRepository
{
    public function getAll($request)
    {
        $limit = env('DEFAULT_PAGING_LENGTH', 15);
        $variants = Variant::orderBy('updated_at', 'DESC')->paginate($limit)->setPath('');
        $variants->appends($request->all());

        return $variants;
    }

    public function getById($id)
    {
        return Variant::find($id);
    }

    public function create($request)
    {
        $variant = new Variant();
        $variant->name = $request->name;

        DB::transaction(function () use ($variant) {
            $variant->save();
        });

        return $variant;
    }

    public function update($id, $request)
    {
        $variant = $this->getById($id);
        $variant->name = $request->name;

        DB::transaction(function () use ($variant) {
            $variant->save();
        });

        return $variant;
    }

    public function delete($id)
    {
        $variant = $this->getById($id);

        DB::transaction(function () use ($variant) {
            $variant->delete();
        });

        return $variant;
    }

    public function isActive($id, $request)
    {
        $variant = $this->getById($id);
        $variant->is_active = filter_var($request->is_active, FILTER_VALIDATE_BOOLEAN);

        DB::transaction(function () use ($variant) {
            $variant->save();
        });

        return $variant;
    }

    public function getAllVariants()
    {
        return Variant::get(['id', 'name']);
    }
}
