<?php

namespace App\Repositories;

use App\Models\Product;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProductRepository
{
    public function getAll($request)
    {
        $limit = env('DEFAULT_PAGING_LENGTH', 15);
        $banners = Product::orderBy('updated_at', 'DESC')->paginate($limit)->setPath('');
        $banners->appends($request->all());

        return $banners;
    }

    public function create($request)
    {
        $user = User::with('store:id,user_id')->find(Auth::id());
        $store_id = $user->store->id;
        $product = new Product();
        $product->store_id = $store_id;
        $product->name = $request->name;
        $product->product_type = $request->product_type;
        $product->details = $request->details;
        if ($request->hasFile('preview_img')) {
            $previewImage = $request->file('preview_img');
            $pathToStore = 'assets/admin/img/products/' . $request->product_type . '/';
            $destinationPath = public_path($pathToStore);
            $originalFile = $previewImage->getClientOriginalName();
            $filename = strtotime(date('Y-m-d H:i:s')) . $originalFile;
            $previewImage->move($destinationPath, $filename);
            $previewImagePath = $pathToStore . $filename;
            $product->preview_img = $previewImagePath;
        }

        DB::transaction(function () use ($product) {
            $product->save();
        });

        return $product;
    }
}
