<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests\BrandRequest;
use App\Repositories\BrandRepository;
use Exception;
use Illuminate\Http\Request;

class BrandController extends AdminController
{
    protected $brandRepository;

    public function __construct(BrandRepository $brandRepository)
    {
        $this->brandRepository = $brandRepository;
    }

    public function index(Request $request)
    {
        $data = $this->brandRepository->getAll($request);

        return view('admin.brands.index', compact('data'));
    }

    public function create()
    {
        return view('admin.brands.create');
    }

    public function store(BrandRequest $request)
    {
        $this->brandRepository->create($request);

        return redirect()->to(route('brands.index'))->with(['message' => 'Brand has been added successfully.']);
    }

    public function edit($id)
    {
        $brand = $this->brandRepository->getById($id);

        return view('admin.brands.edit', compact('brand'));
    }

    public function update(BrandRequest $request, $id)
    {
        $this->brandRepository->update($id, $request);

        return redirect()->to(route('brands.index'))->with(['message' => 'Brand has been updated successfully.']);
    }

    public function destroy($id)
    {
        try {
            $this->brandRepository->delete($id);
        } catch (Exception $ex) {
            return response()->json(['message' => $ex->getMessage()], 422);
        }
    }

    public function isActive(BrandRequest $request, $id)
    {
        $this->brandRepository->isActive($id, $request);
    }
}
