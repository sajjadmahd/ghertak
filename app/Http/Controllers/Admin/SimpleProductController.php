<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Repositories\MasterRepository;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;

class SimpleProductController extends Controller
{
    protected $productRepo, $masterRepo;

    public function __construct(ProductRepository $productRepository, MasterRepository $masterRepository)
    {
        $this->productRepo = $productRepository;
        $this->masterRepo = $masterRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->masterRepo->getAllCategories();
        $brands = $this->masterRepo->getAllBrands();
        return view('admin.products.simple.create', compact('categories', 'brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        dd($request->all());

        // 1. Create product
        $this->productRepo->create($request);
        // 2. Create product combination
        // 3. Attach product brand
        // 4. Attach product category
        // 5. Add product variant
        // 6. Add product variant option
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
