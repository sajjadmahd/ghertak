<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests\VariantOptionRequest;
use App\Repositories\VariantOptionRepository;
use App\Repositories\VariantRepository;
use Exception;
use Illuminate\Http\Request;

class VariantOptionController extends AdminController
{
    protected $variantOptionRepository, $variantRepository;

    public function __construct(VariantOptionRepository $variantOptionRepository, VariantRepository $variantRepository)
    {
        $this->variantOptionRepository = $variantOptionRepository;
        $this->variantRepository = $variantRepository;
    }

    public function index(Request $request)
    {
        $data = $this->variantOptionRepository->getAll($request);

        return view('admin.variantoptions.index', compact('data'));
    }

    public function create()
    {
        $variants = $this->variantRepository->getAllVariants();

        return view('admin.variantoptions.create', compact('variants'));
    }

    public function store(VariantOptionRequest $request)
    {
        $this->variantOptionRepository->create($request);

        return redirect()->to(route('variantoptions.index'))->with(['message' => 'Variant option has been added successfully.']);
    }

    public function edit($id)
    {
        $variantOption = $this->variantOptionRepository->getById($id);

        $variants = $this->variantRepository->getAllVariants();

        return view('admin.variantoptions.edit', compact('variants', 'variantOption'));
    }

    public function update(VariantOptionRequest $request, $id)
    {
        $this->variantOptionRepository->update($id, $request);

        return redirect()->to(route('variantoptions.index'))->with(['message' => 'Variant option has been updated successfully.']);
    }

    public function destroy($id)
    {
        try {
            $this->variantOptionRepository->delete($id);
        } catch (Exception $ex) {
            return response()->json(['message' => $ex->getMessage()], 422);
        }
    }

    public function isActive(VariantOptionRequest $request, $id)
    {
        $this->variantOptionRepository->isActive($id, $request);
    }
}
