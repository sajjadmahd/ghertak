<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests\BannerRequest;
use App\Repositories\BannerRepository;
use Exception;
use Illuminate\Http\Request;

class BannerController extends AdminController
{
    protected $bannerRepository;

    public function __construct(BannerRepository $bannerRepository)
    {
        $this->bannerRepository = $bannerRepository;
    }

    public function index(Request $request)
    {
        $data = $this->bannerRepository->getAll($request);

        return view('admin.banners.index', compact('data'));
    }

    public function create()
    {
        return view('admin.banners.create');
    }

    public function store(BannerRequest $request)
    {
        $this->bannerRepository->create($request);

        return redirect()->to(route('banners.index'))->with(['message' => 'Banner has been added successfully.']);
    }

    public function edit($id)
    {
        $banner = $this->bannerRepository->getById($id);

        return view('admin.banners.edit', compact('banner'));
    }

    public function update(BannerRequest $request, $id)
    {
        $this->bannerRepository->update($id, $request);

        return redirect()->to(route('banners.index'))->with(['message' => 'Banner has been updated successfully.']);
    }

    public function destroy($id)
    {
        try {
            $this->bannerRepository->delete($id);
        } catch (Exception $ex) {
            return response()->json(['message' => $ex->getMessage()], 422);
        }
    }

    public function isActive(BannerRequest $request, $id)
    {
        $this->bannerRepository->isActive($id, $request);
    }
}
