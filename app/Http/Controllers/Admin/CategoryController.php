<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests\CategoryRequest;
use App\Repositories\CategoryRepository;
use Exception;
use Illuminate\Http\Request;

class CategoryController extends AdminController
{
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function index(Request $request)
    {
        $data = $this->categoryRepository->getAll($request);

        return view('admin.categories.index', compact('data'));
    }

    public function create()
    {
        $parentCategories = $this->categoryRepository->getAllParentCategories();

        return view('admin.categories.create', compact('parentCategories'));
    }

    public function store(CategoryRequest $request)
    {
        $this->categoryRepository->create($request);

        return redirect()->to(route('categories.index'))->with(['message' => 'Category has been added successfully.']);
    }

    public function edit($id)
    {
        $category = $this->categoryRepository->getById($id);

        $parentCategories = $this->categoryRepository->getAllParentCategories();

        return view('admin.categories.edit', compact('category', 'parentCategories'));
    }

    public function update(CategoryRequest $request, $id)
    {
        $this->categoryRepository->update($id, $request);

        return redirect()->to(route('categories.index'))->with(['message' => 'Category has been updated successfully.']);
    }

    public function destroy($id)
    {
        try {
            $this->categoryRepository->delete($id);
        } catch (Exception $ex) {
            return response()->json(['message' => $ex->getMessage()], 422);
        }
    }

    public function isActive(CategoryRequest $request, $id)
    {
        $this->categoryRepository->isActive($id, $request);
    }
}
