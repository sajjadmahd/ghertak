<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests\VariantRequest;
use App\Repositories\VariantRepository;
use Exception;
use Illuminate\Http\Request;

class VariantController extends AdminController
{
    protected $variantRepository;

    public function __construct(VariantRepository $variantRepository)
    {
        $this->variantRepository = $variantRepository;
    }

    public function index(Request $request)
    {
        $data = $this->variantRepository->getAll($request);

        return view('admin.variants.index', compact('data'));
    }

    public function create()
    {
        return view('admin.variants.create');
    }

    public function store(VariantRequest $request)
    {
        $this->variantRepository->create($request);

        return redirect()->to(route('variants.index'))->with(['message' => 'Variant has been added successfully.']);
    }

    public function edit($id)
    {
        $variant = $this->variantRepository->getById($id);

        return view('admin.variants.edit', compact('variant'));
    }

    public function update(VariantRequest $request, $id)
    {
        $this->variantRepository->update($id, $request);

        return redirect()->to(route('variants.index'))->with(['message' => 'Variant has been updated successfully.']);
    }

    public function destroy($id)
    {
        try {
            $this->variantRepository->delete($id);
        } catch (Exception $ex) {
            return response()->json(['message' => $ex->getMessage()], 422);
        }
    }

    public function isActive(VariantRequest $request, $id)
    {
        $this->variantRepository->isActive($id, $request);
    }
}
