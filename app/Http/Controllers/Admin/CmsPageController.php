<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests\CmsPageRequest;
use App\Repositories\CmsPageRepository;
use Exception;
use Illuminate\Http\Request;

class CmsPageController extends AdminController
{
    protected $cmsPageRepository;

    public function __construct(CmsPageRepository $cmsPageRepository)
    {
        $this->cmsPageRepository = $cmsPageRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $this->cmsPageRepository->getAll($request);

        return view('admin.cmspages.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parentPages = $this->cmsPageRepository->getAllParentPages();

        return view('admin.cmspages.create', compact('parentPages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CmsPageRequest $request)
    {
        $this->cmsPageRepository->create($request);

        return redirect()->to(route('cmspages.index'))->with(['message' => 'Page has been added successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = $this->cmsPageRepository->getById($id);

        $parentPages = $this->cmsPageRepository->getAllParentPages();

        return view('admin.cmspages.edit', compact('page', 'parentPages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->cmsPageRepository->update($id, $request);

        return redirect()->to(route('cmspages.index'))->with(['message' => 'Page has been updated successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->cmsPageRepository->delete($id);
        } catch (Exception $ex) {
            return response()->json(['message' => $ex->getMessage()], 422);
        }
    }

    public function isActive(CmsPageRequest $request, $id)
    {
        $this->cmsPageRepository->isActive($id, $request);
    }
}
