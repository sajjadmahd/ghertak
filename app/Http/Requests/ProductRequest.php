<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('post')) {
            $rules =  [
                'name' => 'required|min:3|max:100|unique:products,name',
                'sku' => 'required',
                'price' => 'required|numeric|gt:0',
                'available_stock' => 'required|numeric|gt:0',
                'category_id' => 'required|min:1',
                'brand_id' => 'required|min:1',
                'preview_img' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ];
        } elseif ($this->isMethod('put')) {
            if ($this->route()->getActionMethod() == 'isActive') {
                $rules =  [
                    'is_active' => [
                        'required',
                        'in:true,false'
                    ]
                ];
            } else {
                $rules =  [
                    'name' => [
                        'required',
                        'min:3',
                        'max:100',
                        Rule::unique('products')->where(function ($query) {
                            return $query->where('id', '!=', $this->variant);
                        })
                    ],
                    'sku' => 'required',
                    'price' => 'required|numeric|gt:0',
                    'available_stock' => 'required|numeric|gt:0',
                    'category_id' => 'required|min:1',
                    'brand_id' => 'required|min:1',
                    'preview_img' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048'
                ];
            }
        } else {
            $rules = [];
        }

        return $rules;
    }


    public function messages()
    {
        return [
            'name.required' => 'Product name is required.',
            'name.min' => 'Product name must be at least 3 characters.',
            'name.max' => 'Product name may not be greater than 100 characters.',
            'name.unique'   => 'Product name has already been added.'
        ];
    }
}
