<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('post')) {
            $rules =  [
                'image_path' => [
                    'required',
                    'mimes:jpeg,png,jpg,gif,svg',
                    'max:2048'
                ]
            ];
        } elseif ($this->isMethod('put')) {
            if ($this->route()->getActionMethod() == 'isActive') {
                $rules =  [
                    'is_active' => [
                        'required',
                        'in:true,false'
                    ]
                ];
            } else {
                $rules =  [
                    'image_path' => [
                        'mimes:jpeg,png,jpg,gif,svg',
                        'max:2048'
                    ]
                ];
            }
        } else {
            $rules = [];
        }

        return $rules;
    }


    public function messages()
    {
        return [
            'image_path.required' => 'Banner image is required.',
            'image_path.mimes' => 'Banner image must be of type jpeg,png,jpg,gif,svg.',
            'image_path.max' => 'Banner image size must not exceed 2MB.'
        ];
    }
}
