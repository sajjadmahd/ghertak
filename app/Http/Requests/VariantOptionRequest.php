<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VariantOptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('post')) {
            $rules =  [
                'variant_id' => [
                    'required'
                ],
                'name' => [
                    'required',
                    'max:100',
                    Rule::unique('variant_options')->where(function ($query) {
                        return $query->where('variant_id', $this->variant_id);
                    })
                ]
            ];
        } elseif ($this->isMethod('put')) {
            if ($this->route()->getActionMethod() == 'isActive') {
                $rules =  [
                    'is_active' => [
                        'required',
                        'in:true,false'
                    ]
                ];
            } else {
                $rules =  [
                    'variant_id' => [
                        'required'
                    ],
                    'name' => [
                        'required',
                        'max:100',
                        Rule::unique('variant_options')->where(function ($query) {
                            return $query->where('variant_id', $this->variant_id)
                                ->where('id', '!=', $this->variantoption);
                        })
                    ]
                ];
            }
        } else {
            $rules = [];
        }

        return $rules;
    }


    public function messages()
    {
        return [
            'variant_id.required' => 'Variant is required.',
            'name.required' => 'Variant option name is required.',
            'name.min' => 'Variant option name must be at least 3 characters.',
            'name.max' => 'Variant option name may not be greater than 100 characters.',
            'name.unique'   => 'Variant option name has already been added.'
        ];
    }
}
