<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VariantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('post')) {
            $rules =  [
                'name' => [
                    'required',
                    'min:3',
                    'max:100',
                    'unique:variants,name'
                ]
            ];
        } elseif ($this->isMethod('put')) {
            if ($this->route()->getActionMethod() == 'isActive') {
                $rules =  [
                    'is_active' => [
                        'required',
                        'in:true,false'
                    ]
                ];
            } else {
                $rules =  [
                    'name' => [
                        'required',
                        'min:3',
                        'max:100',
                        Rule::unique('variants')->where(function ($query) {
                            return $query->where('id', '!=', $this->variant);
                        })
                    ]
                ];
            }
        } else {
            $rules = [];
        }

        return $rules;
    }


    public function messages()
    {
        return [
            'name.required' => 'Variant name is required.',
            'name.min' => 'Variant name must be at least 3 characters.',
            'name.max' => 'Variant name may not be greater than 100 characters.',
            'name.unique'   => 'Variant name has already been added.'
        ];
    }
}
