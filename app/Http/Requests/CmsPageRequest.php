<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CmsPageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('post')) {
            $rules =  [
                'name' => [
                    'required',
                    'min:3',
                    'max:100',
                    Rule::unique('cms_pages')->where(function ($query) {
                        return $query->where('parent_id', $this->parent_id);
                    })
                ],
                'page_meta_title' => ['required'],
                'page_meta_description' => ['required']

            ];
        } elseif ($this->isMethod('put')) {
            if ($this->route()->getActionMethod() == 'isActive') {
                $rules =  [
                    'is_active' => [
                        'required',
                        'in:true,false'
                    ]
                ];
            } else {
                $rules =  [
                    'name' => [
                        'required',
                        'min:3',
                        'max:100',
                        Rule::unique('cms_pages')->where(function ($query) {
                            return $query->where('parent_id', $this->parent_id);
                        })
                    ],
                    'page_meta_title' => ['required'],
                    'page_meta_description' => ['required']
                ];
            }
        } else {
            $rules = [];
        }

        return $rules;
    }


    public function messages()
    {
        return [
            'name.required' => 'CMS Page name is required.',
            'name.min' => 'CMS Page name must be at least 3 characters.',
            'name.max' => 'CMS Page name may not be greater than 100 characters.',
            'name.unique'   => 'CMS Page name has already been added.'
        ];
    }
}
