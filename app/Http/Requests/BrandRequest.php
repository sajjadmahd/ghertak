<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('post')) {
            $rules =  [
                'name' => [
                    'required',
                    'min:3',
                    'max:100',
                    'unique:brands,name'
                ]
            ];
        } elseif ($this->isMethod('put')) {
            if ($this->route()->getActionMethod() == 'isActive') {
                $rules =  [
                    'is_active' => [
                        'required',
                        'in:true,false'
                    ]
                ];
            } else {
                $rules =  [
                    'name' => [
                        'required',
                        'min:3',
                        'max:100',
                        Rule::unique('brands')->where(function ($query) {
                            return $query->where('id', '!=', $this->brand);
                        })
                    ]
                ];
            }
        } else {
            $rules = [];
        }

        return $rules;
    }


    public function messages()
    {
        return [
            'name.required' => 'Brand name is required.',
            'name.min' => 'Brand name must be at least 3 characters.',
            'name.max' => 'Brand name may not be greater than 100 characters.',
            'name.unique'   => 'Brand name has already been added.'
        ];
    }
}
